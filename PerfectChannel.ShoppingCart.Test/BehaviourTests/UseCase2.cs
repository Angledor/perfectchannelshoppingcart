﻿using NUnit.Framework;
using PerfectChannel.Recruitment.ShoppingCart;
using PerfectChannel.Recruitment.ShoppingCart.Application;
using PerfectChannel.Recruitment.ShoppingCart.Controller;
using PerfectChannel.Recruitment.ShoppingCart.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.ShoppingCart.Test.BehaviourTests
{
    /// <summary>
    /// Behaviour Test of Use Case 2
    /// </summary>
    [TestFixture]
    public class UseCase2
    {
        /// <summary>
        /// 2.    Request to add an item by name or identifier for a specific user's basket.
        /// 3.    Returns the user's basket.
        /// </summary>
        [Test]
        public void AddProductByNameTest()
        {
            var userId = 21;
            var productName = "Apples";
            Basket basket = Services.Root.UserController.AddProductByNameInBasket(userId, productName);

            Assert.IsNotNull(basket);
            Assert.AreEqual(1, basket.Products.Count);
            Assert.AreEqual(1, basket.Products.Single().Product.Id);
            Assert.AreEqual("Apples", basket.Products.Single().Product.Name);
        }

        /// <summary>
        /// 2.    Request to add an item by name or identifier for a specific user's basket.
        /// 3.    Returns the user's basket.
        /// </summary>
        [Test]
        public void AddProductByIdTest()
        {
            var userId = 22;
            var productId = 3;
            Basket basket = Services.Root.UserController.AddProductByIdInBasket(userId, productId);

            Assert.IsNotNull(basket);
            Assert.AreEqual(1,basket.Products.Count);
            Assert.AreEqual(3, basket.Products.Single().Product.Id);
            Assert.AreEqual("Oranges", basket.Products.Single().Product.Name);
        }

        /// <summary>
        /// 4.    To access user's basket, request the basket by user name.
        /// </summary>
        [Test]
        public void GetBasketTest()
        {
            var userId = 23;
            var productName = "Apples";
            Basket basket = Services.Root.UserController.AddProductByNameInBasket(userId, productName);

            Assert.IsNotNull(basket);
            Assert.AreEqual(1, basket.Products.Count);
            Assert.AreEqual(1, basket.Products.Single().Product.Id);
            Assert.AreEqual("Apples", basket.Products.Single().Product.Name);

            Basket basket2 = Services.Root.UserController.GetBasket(userId);

            Assert.IsNotNull(basket2);
            Assert.AreEqual(1, basket2.Products.Count);
            Assert.AreEqual(1, basket.Products.Single().Product.Id);
            Assert.AreEqual("Apples", basket.Products.Single().Product.Name);
        }

        /// <summary>
        /// 5.    Users should not be able to add items which are not in stock.
        /// </summary>
        [Test]
        public void AddTooManyProductsTest()
        {
            var userId = 24;
            var productName = "Apples";

            Basket basket = null;

            // Add product is possible <= 5
            for (int i = 1; i <= 5; i++)
            {
                basket = Services.Root.UserController.AddProductByNameInBasket(userId, productName);
                Assert.IsNotNull(basket);
                Assert.AreEqual(1, basket.Products.Count);
                Assert.AreEqual(i, basket.Products[0].Quantity);
            }

            // Add product is possible > 5
            for (int i = 0; i < 3; i++)
            {
                basket = Services.Root.UserController.AddProductByNameInBasket(userId, productName);
                Assert.IsNotNull(basket);
                Assert.AreEqual(1, basket.Products.Count);
                Assert.AreEqual(5, basket.Products[0].Quantity);
            }
        }
    }
}