﻿using NUnit.Framework;
using PerfectChannel.Recruitment.ShoppingCart;
using PerfectChannel.Recruitment.ShoppingCart.Application;
using PerfectChannel.Recruitment.ShoppingCart.Controller;
using PerfectChannel.Recruitment.ShoppingCart.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.ShoppingCart.Test.BehaviourTests
{
    /// <summary>
    /// Behaviour Test of Use Case 4
    /// </summary>
    [TestFixture]
    public class UseCase4
    {
        /// <summary>
        /// 2.    Request to checkout a user's basket by name
        /// 3.    Return invoice of items.
        /// 4.    When requesting list of items, stock should be reduced accordingly
        /// 5.    Stock should not be allowed to be negative
        /// </summary>
        [Test]
        public void CheckoutBasket()
        {
            // This test start to do the same init as UseCase3

            var userId = 41;
            var qXproducts = new List<QuantityProductId>()
            {
               new QuantityProductId(1, 3),
               new QuantityProductId(2, 15),
               new QuantityProductId(3, 10),
            };

            Basket basket = Services.Root.UserController.GetAddProductsInBasket(userId, qXproducts);

            Assert.IsNotNull(basket);
            Assert.AreEqual(3, basket.Products.Count);
            {
                var qXp1 = basket.Products.SingleOrDefault(P => P.Product.Id == 1);
                Assert.AreEqual(3, qXp1.Quantity);
                Assert.AreEqual("Apples", qXp1.Product.Name);
                
                var qXp2 = basket.Products.SingleOrDefault(P => P.Product.Id == 2);
                // only 10 available 
                Assert.AreEqual(10, qXp2.Quantity); 
                Assert.AreEqual("Bread", qXp2.Product.Name);
                
                var qXp3 = basket.Products.SingleOrDefault(P => P.Product.Id == 3);
                Assert.AreEqual(10, qXp3.Quantity);
                Assert.AreEqual("Oranges", qXp3.Product.Name);
            }

            // Test Core :
            Invoice invoice = Services.Root.UserController.CheckoutBasket(userId);

            // Check invoice
            Assert.IsNotNull(invoice);
            Assert.AreEqual(2.50 * 3 + 1.35 * 10 + 2.99 * 10, invoice.Total);

            // Check stock
            var stock = Services.Root.ProductController.GetAllAvailableProducts();
            
            int expectedProductCountInStock = 3;
            Assert.AreEqual(expectedProductCountInStock, stock.Count());

            {
                /// 4.When requesting list of items, stock should be reduced accordingly
                var qXp1 = stock.SingleOrDefault(P => P.Product.Id == 1);
                Assert.AreEqual(5-3, qXp1.Quantity);
                Assert.AreEqual("Apples", qXp1.Product.Name);

                /// 5.    Stock should not be allowed to be negative
                
                // No Product 2 anymore
                //var qXp2 = stock.SingleOrDefault(P => P.Product.Id == 2);
                //Assert.AreEqual(10-10, qXp2.Quantity);
                //Assert.AreEqual("Bread", qXp2.Product.Name);

                /// 5.    Stock should not be allowed to be negative

                // No Product 3 anymore
                //var qXp3 = stock.SingleOrDefault(P => P.Product.Id == 3);
                //Assert.AreEqual(10-10, qXp3.Quantity);
                //Assert.AreEqual("Oranges", qXp3.Product.Name);
            }
        }
    }
}
