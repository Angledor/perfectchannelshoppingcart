﻿using NUnit.Framework;
using PerfectChannel.Recruitment.ShoppingCart;
using PerfectChannel.Recruitment.ShoppingCart.Application;
using PerfectChannel.Recruitment.ShoppingCart.Controller;
using PerfectChannel.Recruitment.ShoppingCart.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.ShoppingCart.Test.BehaviourTests
{
    /// <summary>
    /// Behaviour Test of Use Case 3
    /// </summary>
    [TestFixture]
    public class UseCase3
    {
        /// <summary>
        /// 2.    Request to add a list of items, specifying their quantity.
        /// 3.    Returns users updated basket.
        /// </summary>
        [Test]
        public void AddProducts()
        {
            var userId = 31;
            var qXproducts = new List<QuantityProductId>()
            {
               new QuantityProductId(1, 3),
               new QuantityProductId(2, 2),
               new QuantityProductId(3, 1),
            };

            Basket basket = Services.Root.UserController.GetAddProductsInBasket(userId, qXproducts);

            Assert.IsNotNull(basket);
            Assert.AreEqual(3, basket.Products.Count);
            {
                var qXp1 = basket.Products.SingleOrDefault(P => P.Product.Id == 1);
                Assert.AreEqual(3, qXp1.Quantity);
                Assert.AreEqual("Apples", qXp1.Product.Name);

                var qXp2 = basket.Products.SingleOrDefault(P => P.Product.Id == 2);
                Assert.AreEqual(2, qXp2.Quantity);
                Assert.AreEqual("Bread", qXp2.Product.Name);

                var qXp3 = basket.Products.SingleOrDefault(P => P.Product.Id == 3);
                Assert.AreEqual(1, qXp3.Quantity);
                Assert.AreEqual("Oranges", qXp3.Product.Name);
            }
        }
    }
}
