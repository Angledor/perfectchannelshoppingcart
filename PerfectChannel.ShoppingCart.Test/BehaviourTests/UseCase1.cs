﻿using NUnit.Framework;
using PerfectChannel.Recruitment.ShoppingCart;
using PerfectChannel.Recruitment.ShoppingCart.Application;
using PerfectChannel.Recruitment.ShoppingCart.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.ShoppingCart.Test.BehaviourTests
{
    /// <summary>
    /// Behaviour Test of Use Case 1
    /// </summary>
    [TestFixture]
    public class UseCase1
    {
        [Test]
        public void GetAllAvailableProductsTest()
        {
            var qXproducts = Services.Root.ProductController.GetAllAvailableProducts();

            int expectedProductCountInStock = 5;
            Assert.AreEqual(expectedProductCountInStock, qXproducts.Count());

            foreach (var qXp in qXproducts)
            {
                Assert.NotNull(qXp.Product.Name);
                Assert.NotNull(qXp.Product.Description);
                Assert.NotNull(qXp.Product.Id);
                Assert.NotNull(qXp.Quantity);
            }

            // Test Product 1 : 1, Apples, Fruit, 5, 2.50
            var qXproduct1 = qXproducts.First();
            Assert.AreEqual("Apples", qXproduct1.Product.Name);
            Assert.AreEqual("Fruit", qXproduct1.Product.Description);
            Assert.AreEqual(1, qXproduct1.Product.Id);
            Assert.AreEqual(5, qXproduct1.Quantity);
        }
    }
}
