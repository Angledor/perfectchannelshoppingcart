﻿using PerfectChannel.Recruitment.ShoppingCart.Application;
using PerfectChannel.Recruitment.ShoppingCart.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace PerfectChannel.ShoppingCart.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Perfect Channel Shopping Cart Console - Welcome");
            MessageLoop();
            WriteLine("Thank You");
        }

        public static void MessageLoop()
        {
            do
            {
                Write("?> ");
            } while (ExecuteCommand(ReadLine()));
        }

        /// <summary>
        /// Execute a command with multiple steps
        /// </summary>
        /// <param name="command">commande string</param>
        /// <returns>False to exit command</returns>
        public static bool ExecuteCommand(string command)
        {
            var cmdParts = command.Split(' ');
            var cmdVerb = cmdParts.First();

            try
            {



                switch (cmdVerb.ToLower())
                {
                    case "q":
                    case "quit":
                        return false;
                    case "h":
                    case "help":
                        DisplayHelp();
                        break;
                    case "list":
                        var qXproducts = Services.Root.ProductController.GetAllAvailableProducts();
                        qXproducts.ToList().ForEach(qXp => WriteLine(qXp));
                        break;
                    case "add":
                        if (cmdParts.Count() != 3)
                            return InvalidCommand(command);
                        {
                            var userId = 0;
                            if (!int.TryParse(cmdParts[1], out userId))
                                return InvalidCommand(command);

                            var productNameOrId = cmdParts[2];
                            var productId = 0;
                            var searchById = int.TryParse(productNameOrId, out productId);

                            var basket = searchById ? Services.Root.UserController.AddProductByIdInBasket(userId, productId)
                                : Services.Root.UserController.AddProductByNameInBasket(userId, productNameOrId);

                            WriteLine($"Add Product {productNameOrId} in basket of user {userId} :");
                            basket.Products.ToList().ForEach(qXp => WriteLine(qXp));
                        }
                        break;
                    case "addbulk":
                        if (cmdParts.Count() < 3)
                            return InvalidCommand(command);
                        {
                            var userId = 0;
                            if (!int.TryParse(cmdParts[1], out userId))
                                return InvalidCommand(command);

                            List<QuantityProductId> products = new List<QuantityProductId>();

                            for (int i = 2; i < cmdParts.Length; i++)
                            {
                                var productStr = cmdParts[i];
                                var productIdStr = productStr.Split('x')[0];
                                var productId = 0;
                                if (!int.TryParse(productIdStr, out productId))
                                    return InvalidCommand(command);

                                var quantityStr = productStr.Split('x')[1];
                                uint quantity = 0;
                                if (!uint.TryParse(quantityStr, out quantity))
                                    return InvalidCommand(command);

                                products.Add(new QuantityProductId(productId, quantity));
                            }

                            var basket = Services.Root.UserController.GetAddProductsInBasket(userId, products);

                            WriteLine($"Add Products in basket of user {userId} :");
                            basket.Products.ToList().ForEach(qXp => WriteLine(qXp));
                        }
                        break;
                    case "basket":
                        if (cmdParts.Count() != 2)
                            return InvalidCommand(command);
                        {
                            var userId = 0;
                            if (!int.TryParse(cmdParts[1], out userId))
                                return InvalidCommand(command);

                            var basket = Services.Root.UserController.GetBasket(userId);

                            WriteLine($"Basket of user {userId} :");
                            basket.Products.ToList().ForEach(qXp => WriteLine(qXp));
                        }
                        break;
                    case "checkout":
                        if (cmdParts.Count() != 2)
                            return InvalidCommand(command);
                        {
                            var userId = 0;
                            if (!int.TryParse(cmdParts[1], out userId))
                                return InvalidCommand(command);

                            var invoice = Services.Root.UserController.CheckoutBasket(userId);

                            WriteLine($"Checkout basket of user {userId} :");
                            WriteLine(invoice);
                        }
                        break;
                    default:
                        InvalidCommand(command);
                        break;
                }
            }
            catch (Exception ex)
            {
                WriteLine($"{command} - command error : {ex.Message}");
                return DisplayHelp();
            }
            return true;
        }

        public static bool InvalidCommand(string command)
        {
            WriteLine($"{command} - command not found");
            DisplayHelp();
            return true;
        }

        public static bool DisplayHelp()
        {
            WriteLine("command help :");
            WriteLine(" quit : quit the console");
            WriteLine(" list : list of all possible items available");
            WriteLine(" add {UserId} {ProductName} :");
            WriteLine("     add an item by name for a specific user's basket");
            WriteLine(" add {UserId} {ProductId} :");
            WriteLine("     add an item by identifier for a specific user's basket");
            WriteLine(" addbulk {UserId} {ProductId}x{ProductQuanity}:");
            WriteLine("     add a list of items, specifying their quantity.");
            WriteLine(" basket {UserId} :");
            WriteLine("     access user's basket");
            WriteLine(" checkout {UserId} :");
            WriteLine("     checkout user's basket");
            return true;
        }

    }
}
