# README - Julien Robberechts Perfect Channel Test #

### How long did you spend on your solution? ###
	* 3h45
		
### How do you compile and run your solution? ###
	* Get source code locally
	* Open the solution in Visual Studio
	* Make sure you get Nuget Package NUnit
	* Run Automatic Tests
	* Run the console project and type these commands for exemple :
			list
			add 1 Apples
			add 1 1
			basket 1
			addbulk 1 1x1 2x2
			checkout 1
		
### What assumptions did you make when went implementing your solution ###
		Some weakness of this code are : (because of limited time and effort)
			- The Repository is only in memory
			- There is no concurrency at all for the momemt
			- There is very few error management
			- I mix UserId and UserName in a single UserId

### Why did you pick the language and design that you did? ###
			- I took C# simple object to focus on : TDD, simple API code, Dependency Injection. 
			
### If you were unable to complete any user stories, outline why and how would you have liked to implement them ###
			- User stories could be added to describe UserId and Name
			- User stories could be added delete items from the basket
			- User security : As a shopper, I can't access to basket of other users.
				why : for obvious privacy
				how : by defining a system of authentication and authorisation
			- salesperson security : As a salesperson, I can access to basket of all users