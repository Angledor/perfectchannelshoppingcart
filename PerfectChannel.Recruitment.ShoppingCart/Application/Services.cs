﻿using PerfectChannel.Recruitment.ShoppingCart.Controller;
using PerfectChannel.Recruitment.ShoppingCart.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.Recruitment.ShoppingCart.Application
{
    public class Services
    {
        private static Services _root = new Services();

        public static Services Root
        {
            get
            {
                if (_root ==null)
                    _root = new Services();
                return _root;
            }
        }

        public IUserRepository UserRepository { get; private set; } = new UsersInMemoryRepository();
        public IProductRepository ProductRepository { get; private set; } = new ProductsInMemoryRepository();

        public IUserController UserController { get; private set; } = new UserController();
        public IProductController ProductController { get; private set; } = new ProductController();

        

    }
}
