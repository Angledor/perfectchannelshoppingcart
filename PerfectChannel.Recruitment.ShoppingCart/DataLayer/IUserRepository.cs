﻿using PerfectChannel.Recruitment.ShoppingCart.Model;

namespace PerfectChannel.Recruitment.ShoppingCart.DataLayer
{
    public interface IUserRepository
    {
        User GetUser(int id);
    }
}