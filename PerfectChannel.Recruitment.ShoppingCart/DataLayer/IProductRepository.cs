﻿using System.Collections.Generic;
using PerfectChannel.Recruitment.ShoppingCart.Model;

namespace PerfectChannel.Recruitment.ShoppingCart.DataLayer
{
    public interface IProductRepository
    {
        IEnumerable<QuantityProduct> GetAllProducts();
        QuantityProduct GetProductByName(string productName);
        QuantityProduct GetProductById(int productId);
        uint DecrementProductStock(int productId, uint quantity);
    }
}