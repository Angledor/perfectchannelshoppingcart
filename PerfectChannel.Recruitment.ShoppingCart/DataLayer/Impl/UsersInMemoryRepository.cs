﻿using PerfectChannel.Recruitment.ShoppingCart.Model;
using PerfectChannel.Recruitment.ShoppingCart.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace PerfectChannel.Recruitment.ShoppingCart.DataLayer
{
    public class UsersInMemoryRepository : IUserRepository
    {
        private List<User> Users { get; set; } = new List<User>();

        public User GetUser(int id)
        {
            var user = Users.SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                user = new User() { Id = id };
                Users.Add(user);
            }
            return user;
        }
    }
}
