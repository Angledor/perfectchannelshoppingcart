﻿using PerfectChannel.Recruitment.ShoppingCart.Model;
using PerfectChannel.Recruitment.ShoppingCart.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace PerfectChannel.Recruitment.ShoppingCart.DataLayer
{
    public class ProductsInMemoryRepository : IProductRepository
    {
        private IEnumerable<QuantityProduct> ProductsStockQuantityDetails
        { get; set; }
            = new List<QuantityProduct>()
                {
                    new QuantityProduct(1, "Apples", "Fruit",5,  2.50),
                    new QuantityProduct(2, "Bread", "Loaf",10,  1.35),
                    new QuantityProduct(3, "Oranges", "Fruit", 10, 2.99),
                    new QuantityProduct(4, "Milk", "Milk", 3,2.07),
                    new QuantityProduct(5, "Chocolate", "Bars",5, 1.79)
                };

        //private IEnumerable<ProductQuantity> ProductsStockQuantity
        //{ get; set; }
        //            = new List<ProductQuantity>()
        //                {
        //            new ProductQuantity(1, 5),
        //            new ProductQuantity(2, 10),
        //            new ProductQuantity(3, 10),
        //            new ProductQuantity(4, 3),
        //            new ProductQuantity(5,  20)
        //                };

        //private IEnumerable<ProductType> ProductsStockDetail
        //{ get; set; }
        //    = new List<ProductType>()
        //        {
        //            new ProductType(1, "Apples", "Fruit",  2.50),
        //            new ProductType(2, "Bread", "Loaf",  1.35),
        //            new ProductType(3, "Oranges", "Fruit",  2.99),
        //            new ProductType(4, "Milk", "Milk", 2.07),
        //            new ProductType(5, "Chocolate", "Bars", 1.79)
        //        };

        public IEnumerable<QuantityProduct> GetAllProducts()
        {
            return ProductsStockQuantityDetails;
        }

        public QuantityProduct GetProductByName(string productName)
        {
            return ProductsStockQuantityDetails.SingleOrDefault(p => string.Compare(p.Product.Name, productName, true) == 0);
        }

        public QuantityProduct GetProductById(int productId)
        {
            return ProductsStockQuantityDetails.SingleOrDefault(p => p.Product.Id == productId);
        }

        public uint DecrementProductStock(int productId, uint requestedQuantity)
        {
            var product = ProductsStockQuantityDetails.SingleOrDefault(p => p.Product.Id == productId);
            var availableQuantity = product.Quantity;
            var quantity = Math.Min(requestedQuantity, availableQuantity);
            product.Quantity -= quantity;
            return quantity;
        }
    }
}
