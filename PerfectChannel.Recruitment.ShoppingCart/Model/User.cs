﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.Recruitment.ShoppingCart.Model
{
    public class User
    {
        public int Id { get; set; }

        public Basket Basket { get; set; } = new Basket();
    }
}
