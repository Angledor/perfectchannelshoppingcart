﻿using PerfectChannel.Recruitment.ShoppingCart.Model;
using System.Collections.Generic;
using System.Linq;

namespace PerfectChannel.Recruitment.ShoppingCart.Model
{
    public class Basket
    {
        public List<QuantityProduct> Products { get; internal set; } = new List<QuantityProduct>();

        public uint GetProductItemsCount(Product product)
        {
            return Products.SingleOrDefault(qXp => qXp.Product.Id == product.Id)?.Quantity ?? 0;
        }

        public void AddProduct(QuantityProduct quantityProduct)
        {
            var qXproduct = Products.SingleOrDefault(qXp => qXp.Product.Id == quantityProduct.Product.Id);
            if (qXproduct == null)
            {
                Products.Add(quantityProduct);
            }
            else
            {
                qXproduct.Quantity += quantityProduct.Quantity;
            }
            
        }
    }
}