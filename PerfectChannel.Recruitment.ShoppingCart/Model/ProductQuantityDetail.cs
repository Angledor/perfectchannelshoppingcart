﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.Recruitment.ShoppingCart.Model
{
    /// <summary>
    /// Quantity + Product Detail
    /// </summary>
    public class QuantityProduct
    {
        public QuantityProduct(Product product, uint quantity)
        {
            Product = product;
            Quantity = quantity;
        }

        public QuantityProduct(int id, string name, string description, uint stock, double price)
        {
            Product = new Product(id, name, description, price);
            Quantity = stock;
        }

        /// <summary>
        /// Type of product
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// The remaining number of item in stock for this type of product
        /// </summary>
        public uint Quantity { get; set; }

        public override string ToString()
        {
            return $"{Product} #Quantity={Quantity}";
        }
    }
}
