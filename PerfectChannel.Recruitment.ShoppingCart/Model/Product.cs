﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.Recruitment.ShoppingCart.Model
{
    /// <summary>
    /// Represent a type of product
    /// </summary>
    public class Product
    {
        public Product(int id, string name, string description, double price)
        {
            Id = id;
            Name = name;
            Description = description;
            Price = price;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }

        public override string ToString()
        {
            return $"[{Id}] {Name} ({Description}) - {Price}";
        }
    }
}
