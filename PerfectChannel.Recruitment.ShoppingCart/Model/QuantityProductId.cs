﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.Recruitment.ShoppingCart.Model
{
    /// <summary>
    /// Quantity + Product Id
    /// </summary>
    public class QuantityProductId
    {
        public QuantityProductId(int productTypeId, uint quantity)
        {
            ProductTypeId = productTypeId;
            Quantity = quantity;
        }

        /// <summary>
        /// Id of the type of product
        /// </summary>
        public int ProductTypeId { get; set; }

        /// <summary>
        /// The remaining number of item in stock for this type of product
        /// </summary>
        public uint Quantity { get; set; }
    }
}
