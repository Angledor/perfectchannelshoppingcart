﻿using System.Collections.Generic;

namespace PerfectChannel.Recruitment.ShoppingCart.Model
{
    /// <summary>
    /// Represent an immutable Invoice
    /// </summary>
    public class Invoice
    {
        public static Invoice CreateInvoice(IEnumerable<QuantityProduct> products)
        {
            double sum = 0.0;
            foreach (var p in products)
            {
                sum += p.Product.Price * p.Quantity;
            }

            return new Invoice() { Total = sum };
        }

        public double Total { get; private set; }

        public override string ToString()
        {
            return $"Invoice Total = {Total}";
        }
    }
}