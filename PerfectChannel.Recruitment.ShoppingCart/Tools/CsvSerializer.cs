﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.Recruitment.ShoppingCart.Tools
{
    public class CsvSerializer
    {
        public readonly char _separator = '\t';

        /// <summary>
        /// Csv Serializer ctor
        /// </summary>
        /// <param name="separator">Csv Separator used to Serialize or Deserialize</param>
        public CsvSerializer(char separator)
        {
            _separator = separator;
        }

        /// <summary>
        /// Csv Separator used to Serialize or Deserialize.
        /// </summary>
        public char Separator { get { return _separator; } }

        /// <summary>
        /// Deserialize from csv format : split a Csv string to extract multiple text columns
        /// </summary>
        /// <param name="csvString">Csv string</param>
        /// <returns>Array of text columns</returns>
        public string[] Deserialize(string csvString)
        {
            if (csvString == null)
            {
                throw new ApplicationException("null text is not a valid CSV string");
            }

            return csvString.Split(_separator);
        }

        /// <summary>
        /// Serialize into csv format : aggregate multiple text columns to format a Csv string
        /// </summary>
        /// <param name="columns">Array of text columns</param>
        /// <returns>Serialized Csv string</returns>
        public string Serialize(params string[] columns)
        {
            return string.Join(_separator.ToString(), columns);
        }

        /// <summary>
        /// Serialize into csv format : aggregate multiple text columns to format a Csv string
        /// </summary>
        /// <param name="columns">Array of text columns</param>
        /// <returns>Serialized Csv string</returns>
        public string Serialize(IEnumerable<string> columns)
        {
            return string.Join(_separator.ToString(), columns);
        }
    }
}
