﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PerfectChannel.Recruitment.ShoppingCart.Tools
{
    /// <summary>
    /// CSV Reader
    /// </summary>
    public class CsvReader : IDisposable
    {
        private StreamReader _readerStream = null;
        public readonly CsvSerializer _csvSerializer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvSerializer"></param>
        public CsvReader(CsvSerializer csvSerializer)
        {
            _csvSerializer = csvSerializer;
        }

        /// <summary>
        /// Open the Csv File for reading
        /// </summary>
        /// <param name="fileName">File path of the existing csv file to open</param>
        public void Open(string fileName)
        {
            _readerStream = File.OpenText(fileName);
        }

        /// <summary>
        /// Read N columns on the current line of the Csv file
        /// </summary>
        /// <returns>Array of column</returns>
        public string[] Read()
        {
            /// Read input
            string line;
            line = ReadLine();

            if (line == null)
            {
                return new string[0];
            }

            /// split Csv input
            return _csvSerializer.Deserialize(line);
        }

        private string ReadLine()
        {
            return _readerStream.ReadLine();
        }

        public void Close()
        {
            Dispose(true);
        }

        #region IDisposable Support

        // To detect multiple calls
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // free managed resources
                    if (_readerStream != null)
                    {
                        _readerStream.Dispose();
                        _readerStream = null;
                    }
                }

                // free native resources : none

                disposedValue = true;
            }
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}

