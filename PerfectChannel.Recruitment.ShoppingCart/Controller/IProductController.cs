﻿using PerfectChannel.Recruitment.ShoppingCart.Model;
using System.Collections.Generic;

namespace PerfectChannel.Recruitment.ShoppingCart.Controller
{
    public interface IProductController
    {
        IEnumerable<QuantityProduct> GetAllAvailableProducts();
    }
}