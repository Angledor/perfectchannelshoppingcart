﻿using PerfectChannel.Recruitment.ShoppingCart.Model;
using System.Collections.Generic;

namespace PerfectChannel.Recruitment.ShoppingCart.Controller
{
    public interface IUserController
    {
        User GetUser(int userId);

        Basket AddProductByNameInBasket(int userId, string productName);

        Basket AddProductByIdInBasket(int userId, int productId);

        Basket GetBasket(int userId);

        Basket GetAddProductsInBasket(int userId, IList<QuantityProductId> products);

        Invoice CheckoutBasket(int userId);
    }
}