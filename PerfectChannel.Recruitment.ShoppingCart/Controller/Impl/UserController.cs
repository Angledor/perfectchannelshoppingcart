﻿using PerfectChannel.Recruitment.ShoppingCart.Application;
using PerfectChannel.Recruitment.ShoppingCart.DataLayer;
using PerfectChannel.Recruitment.ShoppingCart.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.Recruitment.ShoppingCart.Controller
{
    public class UserController : IUserController
    {
        /// <summary>
        /// Get all available type of product with stock information
        /// </summary>
        /// <returns></returns>
        public IEnumerable<QuantityProduct> Add()
        {
            return Services.Root.ProductRepository.GetAllProducts();
        }

        public User GetUser(int userId)
        {
            return Services.Root.UserRepository.GetUser(userId);
        }

        public Basket AddProductByNameInBasket(int userId, string productName)
        {
            User user = GetUser(userId);
            QuantityProduct productStock = Services.Root.ProductRepository.GetProductByName(productName);
            return AddProducInBasket(user, productStock, 1);
        }

        public Basket AddProductByIdInBasket(int userId, int productId)
        {
            User user = GetUser(userId);
            QuantityProduct productStock = Services.Root.ProductRepository.GetProductById(productId);
            return AddProducInBasket(user, productStock, 1);
        }

        public Basket GetBasket(int userId)
        {
            User user = GetUser(userId);
            return user.Basket;
        }

        public Basket GetAddProductsInBasket(int userId, IList<QuantityProductId> products)
        {
            User user = GetUser(userId);

            foreach (var productRef in products)
            {
                QuantityProduct productStock = Services.Root.ProductRepository.GetProductById(productRef.ProductTypeId);
                AddProducInBasket(user, productStock, productRef.Quantity);
            }

            return user.Basket;
        }

        public Invoice CheckoutBasket(int userId)
        {
            User user = GetUser(userId);

            var basketProducts = user.Basket.Products;
            var boughtProducts = new List<QuantityProduct>();
            var stock = Services.Root.ProductRepository.GetAllProducts();

            foreach (var product in basketProducts)
            {
                var stockProduct = stock.SingleOrDefault(p => p.Product.Id == product.Product.Id);

                /// Decrease Stock
                var actualQuantity = Services.Root.ProductRepository.DecrementProductStock(product.Product.Id, product.Quantity);

                // order this product (including quantity = 0)
                boughtProducts.Add(new QuantityProduct(product.Product, actualQuantity));
            }

            return Invoice.CreateInvoice(boughtProducts);
        }

        #region private methods

        private Basket AddProducInBasket(User user, QuantityProduct productStock, uint requestedQuantity)
        {
            if (productStock == null && productStock.Quantity == 0)
                return user.Basket;

            var existingQuantityInBasket = user.Basket.GetProductItemsCount(productStock.Product);

            var actualQuantity = Math.Min(existingQuantityInBasket + requestedQuantity, productStock.Quantity) - existingQuantityInBasket;

            user.Basket.AddProduct(new QuantityProduct(productStock.Product, actualQuantity));

            return user.Basket;
        }

        #endregion
    }
}
