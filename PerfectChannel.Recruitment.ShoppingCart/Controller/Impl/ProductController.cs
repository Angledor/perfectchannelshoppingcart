﻿using PerfectChannel.Recruitment.ShoppingCart.Application;
using PerfectChannel.Recruitment.ShoppingCart.DataLayer;
using PerfectChannel.Recruitment.ShoppingCart.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannel.Recruitment.ShoppingCart.Controller
{
    public class ProductController : IProductController
    {
        /// <summary>
        /// Get all available type of product with stock information
        /// </summary>
        /// <returns></returns>
        public IEnumerable<QuantityProduct> GetAllAvailableProducts()
        {
            return Services.Root.ProductRepository.GetAllProducts().Where(qXp => qXp.Quantity > 0);
        }
    }
}
